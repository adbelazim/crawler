# -*- coding: utf-8 -*-
from selenium import webdriver
from bs4 import BeautifulSoup
import json

NUMBER_PAGES = 500
browser = webdriver.Chrome(executable_path=r"/Users/cristobal/Documents/Crawler/test_craw/chromedriver")


def write_file(list_json, filename):
	with open(filename, 'w') as f:
		f.write(json.dumps(list_json, ensure_ascii = False))

#list for documents
classifieds_save = []
for i in range(1, NUMBER_PAGES):
	print("Work in page %d" %i)
	browser.get('https://www.computrabajo.cl/empleos-en-r.metropolitana?p=' + str(i))
	page_source = BeautifulSoup(browser.page_source,'html.parser')
	#print(page_source)
	classifieds = page_source.find_all('div',class_='bRS bClick ')

	
	for c in classifieds:
		clsf_save = {}
		#Here is all the classified data
		c_data = c.find('div', class_='iO')

		#Meta data of the classified
		#title
		c_title = c_data.find('h2', class_='tO').text
		#description
		c_description = c_data.p.text

		#organization data
		org = c_data.find('div', class_='w_100 fl mtb5 lT')

		#organization name
		try:
			org_name = org.find('span',itemprop='name').a.text
		except:
			org_name = 'NA'
		
		#information of region
		try:
			org_region = org.find('span',itemprop='addressLocality').a.text
		except:
			org_region = 'NA'
		
		#information of the city
		try:
			org_city = org.find('span',itemprop='addressRegion').a.text
		except:
			org_city = 'NA'
		
		#valorations of the classified
		try:
			org_val = org.find('span',class_='valoracions').string.replace('\n', '')
		except:
			org_val = 'NA'


		clsf_save['title'] = c_title
		clsf_save['description'] = c_description
		clsf_save['organization_name'] = org_name
		clsf_save['organization_region'] = org_region
		clsf_save['organization_city'] = org_city
		clsf_save['valoration'] = org_val
		
		# print("------->")
		# print("classified", clsf_save)
		# print("------->")

		classifieds_save.append(clsf_save)

print("Number of documents %i" %len(classifieds_save))
write_file(classifieds_save, "classifieds.json")

	# classifieds_od = page_source.find_all('div',class_='bRS bClick oD')

	# classifieds_ou = page_source.find_all('div',class_='bRS bClick  oU')

	# classifieds_od_ou = page_source.find_all('div',class_='bRS bClick oD oU')
	

